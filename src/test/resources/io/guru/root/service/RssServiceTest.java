package io.guru.root.service;

import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import io.guru.root.entity.Item;
import io.guru.root.exception.RssException;

public class RssServiceTest {

	private RssService rssService; 
	
	@Before
	public void setUp() throws Exception {
		rssService= new RssServiceTest();
	}

	@Test
	public void testGetItemsFile() throws RssException {
		List<Item> items=	rssService.getItems(new File("test-rss/javavids.xml"));
		assertEquals(10, items.size());
		
	}

}
