/**
 * 
 */
package io.guru.root.exception;

/**
 * @author hanuman
 *
 */
public class RssException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public RssException(Throwable cause) {
		super(cause);
	}

}
