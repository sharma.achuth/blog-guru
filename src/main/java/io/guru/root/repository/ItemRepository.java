/**
 * 
 */
package io.guru.root.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import io.guru.root.entity.Blog;
import io.guru.root.entity.Item;

/**
 * @author hanuman
 *
 */
public interface ItemRepository  extends JpaRepository<Item
,Long>{

	List<Item> findByBlog(Blog blog,Pageable pageable);
	Item findByBlogAndLink(Blog blog, String link);

}
