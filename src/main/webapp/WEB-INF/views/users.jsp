<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../layout/taglib.jsp"%>

<div class="component">
	<table
		class="table table-hover table-striped  table-bordered table-hover">
		<tr>
			<th>user name</th>
			<th>user operations</th>
		</tr>
		<c:forEach items="${users}" var="user">
			<tr>
				<td><a href="<spring:url value='/users/${user.id}.html'/>">
						<c:out value="${user.name} " />
				</a></td>
				<td><a href="<spring:url value='/users/remove/${user.id}'/>"
					class="btn btn-danger triggerRemove"> remove ${user.name} </a></td>
			</tr>
		</c:forEach>
	</table>

</div>


<!-- Modal -->
<div class="modal fade" id="modalRemove" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Remove Blog</h4>
			</div>
			<div class="modal-body">really remove?</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">cancle</button>
				<a href="" class="btn btn-danger removeBtn">Remove</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {

		$(".triggerRemove").click(function(e) {
			alert("56544545454545454");
			e.preventDefault();
			$("#modalRemove .removeBtn").attr("href", $(this).attr("href"));
			$("#modalRemove").modal();
		});

	});
</script>
