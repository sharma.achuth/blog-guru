<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../layout/taglib.jsp"%>
<div class="container">
	<c:if test="${param.success eq true }">
		<div class="alert alert-success">Regestration successful</div>
	</c:if>
	<form:form commandName="user"
		cssClass="form-horizontal registrationForm" action="register.html">
		<div class="form-group">
			<label for="name" class="col-sm-2 control-label">name</label>
			<div class="col-sm-10">
				<form:input cssClass="form-control" id="name" placeholder="name"
					path="name" />
				<form:errors path="name" />
			</div>
		</div>

		<div class="form-group">
			<label for="email" class="col-sm-2 control-label">email</label>
			<div class="col-sm-10">
				<form:input cssClass="form-control" id="inputEmail3"
					placeholder="name" path="email" />
				<form:errors path="email" />
			</div>
		</div>

		<div class="form-group">
			<label for="password" class="col-sm-2 control-label">password</label>
			<div class="col-sm-10">
				<form:password cssClass="form-control" id="password"
					placeholder="password" path="password" />
				<form:errors path="password" />
			</div>
		</div>

		<div class="form-group">
			<label for="password" class="col-sm-2 control-label">password
				again</label>
			<div class="col-sm-10">
				<input type="password" name="password_again" id="password_again"
					Class="form-control">
			</div>
		</div>


		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-default">Sign in</button>
			</div>
		</div>

	</form:form>
</div>
<script type="text/javascript">
<!--
	//-->
	$(document)
			.ready(
					function() {
						$(".registrationForm")
								.validate(
										{
											rules : {
												name : {
													required : true,
													minlength : 3,
													remote : {
														url : "<spring:url value='/register/available.html' />",
														type : "get",
														data : {
															userName : function() {
																return $(
																		"#name")
																		.val();
															}
														}
													}
												},
												email : {
													required : true,
													email : true
												},
												password : {
													required : true,
													minlength : 3
												},
												password_again : {
													required : true,
													minlength : 3,
													equalTo : "#password"
												}
											},
											highlight : function(element) {
												$(element).closest(
														".form-group")
														.removeClass(
																"has-success")
														.addClass("has-error");
											},
											unhighlight : function(element) {
												$(element)
														.closest(".form-group")
														.removeClass(
																"has-error")
														.addClass("has-success");
											},
											messages : {
												name : {
													remote : "such username already exist"
												}
											}
										});
					});
</script>
