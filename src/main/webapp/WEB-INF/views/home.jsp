<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../layout/taglib.jsp"%>

<h1>Latest new from java world :</h1>

<table
	class="table table-hover table-striped table-bordered table-hover">
	<tr>
		<th>id</th>
		<th>publishedDate</th>

		<th>title</th>

		<th>description</th>



		<th>link</th>
	</tr>
	<c:forEach items="${items}" var="item">
		<tr>
			<td>${item.id }</td>
			<td>${item.publishedDate }<br> <c:out
					value="${item.blog.name}"/></td>

			<td>${item.title }</td>

			<td>${item.description }</td>


			<td>${item.link }</td>
		</tr>

	</c:forEach>
</table>